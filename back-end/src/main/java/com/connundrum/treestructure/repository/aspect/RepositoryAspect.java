package com.connundrum.treestructure.repository.aspect;

import com.connundrum.treestructure.repository.exceptions.NotFoundException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 *  Aspect for throwing NotFoundException if repository returns null object and suppose to find some.
 *  If repository method can return null value the return type should be wrapped by Optional class.
 */
@Aspect
@Component
public class RepositoryAspect {

    /**
     * A method find poincut operation is the execution of any find method defined in any
     * repository interface. This definition assumes that interfaces are placed in the
     * "repository" package, and the method name starts from "find".
     */
    @Pointcut("execution(* com.connundrum.treestructure.repository.*.find*(..))")
    public void methodFindPointcut() {
    }

    /**
     * A method delete poincut operation is the execution of any delete method defined in any
     * repository interface. This definition assumes that interfaces are placed in the
     * "repository" package, and the method name starts from "delete".
     */
    @Pointcut("execution(* com.connundrum.treestructure.repository.*.delete*(..))")
    public void methodDeletePointcut() {
    }


    /**
     * Check result of invocation delete method. Because delete method have a parameter that is not null
     * the return value should be long value >0. If this is not the case throw NotFoundException.
     * @param joinPoint method invocation joint point
     * @param returnValue return value to check
     * @throws Throwable
     */
    @AfterReturning(value = "methodDeletePointcut())", returning = "returnValue")
    public void afterDelete(JoinPoint joinPoint, Object returnValue) throws Throwable {
        Class target = ((MethodSignature) joinPoint.getSignature()).getReturnType();
        if(!target.equals(Optional.class)){
            if(((Long)returnValue) <= 0)
                throw new NotFoundException(joinPoint.getSignature().getName(), joinPoint.getArgs().toString());
        }

    }

    /**
     * Check result of invocation find method. If find method return type is not wrapped by Optional we assume
     * the return value should be not null. If this is not the case throw NotFoundException.
     * @param joinPoint method invocation joint point
     * @param returnValue return value to check
     * @throws Throwable
     */
    @AfterReturning(value = "methodFindPointcut())", returning = "returnValue")
    public void afterFind(JoinPoint joinPoint, Object returnValue) throws Throwable {
        Class target = ((MethodSignature) joinPoint.getSignature()).getReturnType();
        if(!target.equals(Optional.class)){
            if(returnValue == null)
                throw new NotFoundException(joinPoint.getSignature().getName(), joinPoint.getArgs().toString());
        }

    }

}
