package com.connundrum.treestructure.repository;

import com.connundrum.treestructure.model.entity.TreeNodeStructureEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TreeNodeStructureRepository extends JpaRepository<TreeNodeStructureEntity, Long>, TreeNodeStructureRepositoryCustom{

    TreeNodeStructureEntity findByPublicId(String publicId);
    Long deleteByPublicId(String publicId);

}
