package com.connundrum.treestructure.repository.impl;

import com.connundrum.treestructure.model.entity.QTreeNodeStructureEntity;
import com.connundrum.treestructure.model.entity.TreeNodeStructureEntity;
import com.connundrum.treestructure.repository.TreeNodeStructureRepositoryCustom;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TreeNodeStructureRepositoryImpl extends QueryDslRepositorySupport implements TreeNodeStructureRepositoryCustom {

    private static final QTreeNodeStructureEntity treeNodestructureEntity = QTreeNodeStructureEntity.treeNodeStructureEntity;
    
    public TreeNodeStructureRepositoryImpl() {
        super(TreeNodeStructureEntity.class);
    }

    @Override
    public List<TreeNodeStructureEntity> findRoots() {
        //use Query DSL for finding roots
        return from(treeNodestructureEntity).where(treeNodestructureEntity.parent.isNull()).list(treeNodestructureEntity);
    }


}
