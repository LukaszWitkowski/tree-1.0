package com.connundrum.treestructure.repository.exceptions;

/**
 * Exception thrown in case we could not find object in database that should be there.
 */
public class NotFoundException extends RuntimeException {

    private static final String msg = "%s with publicId = %s not found";

    public NotFoundException(String resourceName, String publicId) {
        super(String.format(msg, resourceName, publicId));
    }

}
