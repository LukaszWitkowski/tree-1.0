package com.connundrum.treestructure.repository;

import com.connundrum.treestructure.model.entity.TreeNodeStructureEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TreeNodeStructureRepositoryCustom{

    /**
     * Find all root nodes for trees in database.
     * @return List of root nodes.
     */
    List<TreeNodeStructureEntity> findRoots();
}
