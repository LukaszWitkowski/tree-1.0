package com.connundrum.treestructure.model.gui;

import javax.validation.constraints.NotNull;

/**
 * Model for communication with front-end application. Used in adding and updating nodes in tree.
 */
public class TreeNodeModel {

    private String publicId;

    @NotNull
    private Long value;

    private Long sum;

    private String parentPublicId;

    public TreeNodeModel() {
    }

    public TreeNodeModel(String parentPublicId, String publicId, Long value, Long sum) {
        this.parentPublicId = parentPublicId;
        this.publicId = publicId;
        this.value = value;
        this.sum = sum;
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public Long getSum() {
        return sum;
    }

    public void setSum(Long sum) {
        this.sum = sum;
    }

    public String getParentPublicId() {
        return parentPublicId;
    }

    public void setParentPublicId(String parentPublicId) {
        this.parentPublicId = parentPublicId;
    }
}
