package com.connundrum.treestructure.model.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "treenodestructure")
public class TreeNodeStructureEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "public_id", nullable = false)
    private String publicId;

    @Column(name= "value", nullable = false)
    private Long value;

    @OneToOne
    private TreeNodeStructureEntity parent;

    @OneToMany(cascade=CascadeType.ALL,orphanRemoval=true,mappedBy="parent")
    private List<TreeNodeStructureEntity> nextLevelNodes = new ArrayList<TreeNodeStructureEntity>();

    public TreeNodeStructureEntity() {
    }

    public TreeNodeStructureEntity(Long id, String publicId, Long value, TreeNodeStructureEntity parent, List<TreeNodeStructureEntity> nextLevelNodes) {
        this.id = id;
        this.publicId = publicId;
        this.value = value;
        this.parent = parent;
        this.nextLevelNodes = nextLevelNodes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public TreeNodeStructureEntity getParent() {
        return parent;
    }

    public void setParent(TreeNodeStructureEntity parent) {
        this.parent = parent;
    }

    public List<TreeNodeStructureEntity> getNextLevelNodes() {
        return nextLevelNodes;
    }

    public void setNextLevelNodes(List<TreeNodeStructureEntity> nextLevelNodes) {
        this.nextLevelNodes = nextLevelNodes;
    }
}
