package com.connundrum.treestructure.model.gui;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Model for communication with front-end application. Used in getting tree structure from back-end application.
 */
public class TreeStructureModel {

    @NotNull
    private TreeNodeModel node;
    private List<TreeStructureModel> children;

    public TreeStructureModel() {
    }

    public TreeStructureModel(TreeNodeModel node, List<TreeStructureModel> children) {
        this.node = node;
        this.children = children;
    }

    public TreeNodeModel getNode() {
        return node;
    }

    public void setNode(TreeNodeModel node) {
        this.node = node;
    }

    public List<TreeStructureModel> getChildren() {
        return children;
    }

    public void setChildren(List<TreeStructureModel> children) {
        this.children = children;
    }
}
