package com.connundrum.treestructure.controller;

import com.connundrum.treestructure.model.gui.TreeNodeModel;
import com.connundrum.treestructure.model.gui.TreeStructureModel;
import com.connundrum.treestructure.repository.exceptions.NotFoundException;
import com.connundrum.treestructure.service.TreeStructureService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Controller for CRUD methods.
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/treeStructureService", produces = MediaType.APPLICATION_JSON_VALUE)
public class TreeStructureController {

    private static final Logger LOGGER  = LoggerFactory.getLogger(TreeStructureController.class);

    @Autowired
    private TreeStructureService treeStructureService;

    @ApiOperation(value="Get object by public Id", notes = "Gets a sub-`Tree' object by publicId. \n")
    @RequestMapping(value = "/treeStructure/{publicId}", method = RequestMethod.GET)
    public TreeStructureModel getByPublicId(
            @ApiParam(value = "TreeStructure public id.")
            @PathVariable("publicId") String publicId) {
        TreeStructureModel treeStructure = treeStructureService.getByPublicId(publicId);
        return treeStructure;
    }

    @ApiOperation(value="Get all objects", notes = "Gets all `Tree' objects. \n")
    @RequestMapping(value = "/treeStructure/", method = RequestMethod.GET)
    public List<TreeStructureModel> get() {
    	List<TreeStructureModel>treeStructures = treeStructureService.get();
        return treeStructures;
    }

    @ApiOperation(value="Create node", notes = "Creates a single 'Node' for tree object. \n")
    @RequestMapping(value = "/treeStructure/node", method = RequestMethod.POST)
    public ResponseEntity<Void> create(
            @ApiParam(value = "Node model that should be added to tree.")
            @RequestBody @Valid @NotNull TreeNodeModel treeNode) {
    	String publicId = treeStructureService.createNode(treeNode);
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        headers.add("Location", publicId);
       	return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @ApiOperation(value="Update node", notes = "Updates a single `Node' tree object. \n")
    @RequestMapping(value = "/treeStructure/node/{publicId}", method = RequestMethod.PUT)
    public ResponseEntity<Void>  update(
            @ApiParam(value = "Public id of node to be updated.")
            @PathVariable("publicId") String publicId,
            @ApiParam(value = "Model of updated node.")
            @RequestBody @NotNull TreeNodeModel treeNode) {
        treeStructureService.updateNode(treeNode);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @ApiOperation(value="Delete node", notes = "Deletes a single `Node' object. \n")
    @RequestMapping(value = "/treeStructure/node/{publicId}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(
            @ApiParam(value = "Public id of node to delete.")
            @PathVariable("publicId") String publicId) {
        treeStructureService.deleteNode(publicId);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public  ResponseEntity<String> handleException(Exception e) {
        LOGGER.error(e.getMessage());
        return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
