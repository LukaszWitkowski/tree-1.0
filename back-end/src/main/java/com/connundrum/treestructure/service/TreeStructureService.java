package com.connundrum.treestructure.service;

import com.connundrum.treestructure.model.gui.TreeNodeModel;
import com.connundrum.treestructure.model.gui.TreeStructureModel;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TreeStructureService {

        TreeStructureModel getByPublicId(String publicId);

        List<TreeStructureModel> get();

        String createNode(TreeNodeModel treeNode);

        void updateNode(TreeNodeModel treeNode);

        void deleteNode(String publicId);
}
