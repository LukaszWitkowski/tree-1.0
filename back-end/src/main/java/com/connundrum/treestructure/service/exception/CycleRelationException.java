package com.connundrum.treestructure.service.exception;


public class CycleRelationException extends RuntimeException {

    public CycleRelationException(String message) {
        super(message);
    }
}
