package com.connundrum.treestructure.service.converter;

import com.connundrum.treestructure.model.entity.TreeNodeStructureEntity;
import com.connundrum.treestructure.model.gui.TreeNodeModel;
import com.connundrum.treestructure.model.gui.TreeStructureModel;
import com.connundrum.treestructure.repository.TreeNodeStructureRepository;
import com.connundrum.treestructure.service.exception.CycleRelationException;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Class to convert Entity <-> Model
 */
public class Converter {

    /**
     * Convert model send by front-end applications to database entity.
     * @param model model to transform
     * @param repository tree node repository for collecting information about dependent nodes
     * @return database entity
     */
    public static TreeNodeStructureEntity convertNodeModelToEntity(TreeNodeModel model, TreeNodeStructureRepository repository){
        TreeNodeStructureEntity treeNodeStructureEntity = new TreeNodeStructureEntity();
        treeNodeStructureEntity.setPublicId(model.getPublicId());
        if(model.getParentPublicId() != null)
            trySetParentNode(model, repository, treeNodeStructureEntity);
        treeNodeStructureEntity.setValue(model.getValue());
        return treeNodeStructureEntity;
    }

    /**
     * Check if we not try to attach node to its subtree.
     */
    private static void trySetParentNode(TreeNodeModel model, TreeNodeStructureRepository repository, TreeNodeStructureEntity treeNodeStructureEntity) {
        checkNodeAssigment(model.getParentPublicId(), model.getPublicId(), repository);
        treeNodeStructureEntity.setParent(repository.findByPublicId(model.getParentPublicId()));
    }

    /**
     * Throw exception if we try to make cycle in our graph.
     */
    private static void checkNodeAssigment(String parentPublicId, String publicId, TreeNodeStructureRepository repository) {
        if(publicId.equals(parentPublicId))
            throw new CycleRelationException("Try to attach node to its subtree");
        if(parentPublicId != null) {
            TreeNodeStructureEntity parent = repository.findByPublicId(parentPublicId).getParent();
            if(parent!=null)
                checkNodeAssigment(parent.getPublicId(), publicId, repository);
        }
    }

    /**
     * Convert entity to tree structure model, for getting information about whole tree structure.
     * @param entity entity to transform
     * @return tree structure model
     */
    public static TreeStructureModel convertEntityToTreeModel(TreeNodeStructureEntity entity){
        List<TreeStructureModel> collect = entity.getNextLevelNodes().stream().map(Converter::convertEntityToTreeModel).collect(Collectors.toList());
        return new TreeStructureModel(convertEntityToNodeModel(entity), collect);
    }

    /**
     * Convert entity to tree node model, for getting information about given node.
     * @param entity entity to transform
     * @return tree node model
     */
    public static TreeNodeModel convertEntityToNodeModel(TreeNodeStructureEntity entity){
        return new TreeNodeModel(entity.getParent()!=null ? entity.getParent().getPublicId() : null, entity.getPublicId(), entity.getValue(), entity.getNextLevelNodes().size()==0 ? countSumForEntity(entity) : null);
    }

    /**
     * Count sum value for node, as a sum of values from this node up to the root.
     * @param entity entity that we want to count sum value to the root
     * @return
     */
    private static Long countSumForEntity(TreeNodeStructureEntity entity) {
        if(entity.getParent() == null)
            return entity.getValue();
        return entity.getValue()+countSumForEntity(entity.getParent());
    }
}
