package com.connundrum.treestructure.service.impl;

import com.connundrum.treestructure.model.entity.TreeNodeStructureEntity;
import com.connundrum.treestructure.model.gui.TreeNodeModel;
import com.connundrum.treestructure.model.gui.TreeStructureModel;
import com.connundrum.treestructure.repository.TreeNodeStructureRepository;
import com.connundrum.treestructure.service.TreeStructureService;
import com.connundrum.treestructure.service.converter.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TreeStructureServiceImpl implements TreeStructureService {


    @Autowired
    private TreeNodeStructureRepository treeNodeStructureRepository;

    @Override
    public List<TreeStructureModel> get() {
        List<TreeNodeStructureEntity> list = treeNodeStructureRepository.findRoots();
        return list.stream().map(Converter::convertEntityToTreeModel).collect(Collectors.toList());
    }

    @Override
    public TreeStructureModel getByPublicId(@NotNull String publicId) {
        return Converter.convertEntityToTreeModel(treeNodeStructureRepository.findByPublicId(publicId));
    }

    @Override
    @Transactional
    public String createNode(@NotNull TreeNodeModel treeNodeModel) {
        if (StringUtils.isEmpty(treeNodeModel.getPublicId())) {
            treeNodeModel.setPublicId(UUID.randomUUID().toString());
        }

        TreeNodeStructureEntity treeNodeStructureEntity = Converter.convertNodeModelToEntity(treeNodeModel, treeNodeStructureRepository);

        treeNodeStructureRepository.save(treeNodeStructureEntity);

        return treeNodeStructureEntity.getPublicId();
    }

    @Override
    @Transactional
    public void updateNode(@NotNull TreeNodeModel treeNode) {
        TreeNodeStructureEntity byPublicId = treeNodeStructureRepository.findByPublicId(treeNode.getPublicId());
        TreeNodeStructureEntity treeNodeStructureEntity = Converter.convertNodeModelToEntity(treeNode, treeNodeStructureRepository);
        treeNodeStructureEntity.setId(byPublicId.getId());
        treeNodeStructureEntity.setNextLevelNodes(byPublicId.getNextLevelNodes());
        treeNodeStructureRepository.save(treeNodeStructureEntity);
    }

    @Override
    @Transactional
    public void deleteNode(@NotNull String publicId) {
        treeNodeStructureRepository.deleteByPublicId(publicId);
    }


}
