package com.connundrum.treestructure.service.converter;

import com.connundrum.treestructure.model.entity.TreeNodeStructureEntity;
import com.connundrum.treestructure.model.gui.TreeNodeModel;
import com.connundrum.treestructure.model.gui.TreeStructureModel;
import com.connundrum.treestructure.repository.TreeNodeStructureRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.emptyCollectionOf;
import static org.hamcrest.Matchers.equalTo;


@RunWith(MockitoJUnitRunner.class)
public class ConverterTest {

    @Mock
    private TreeNodeStructureRepository repository;

    @Test
    public void should_ReturnNode_When_GivenEntityIsEmpty() {
        // given
        // when
        TreeNodeModel model = Converter.convertEntityToNodeModel(new TreeNodeStructureEntity());
        // then
        Assert.assertNotNull(model);
    }

    @Test
    public void should_ReturnTree_When_GivenEntityIsEmpty() {
        // given
        // when
        TreeStructureModel model = Converter.convertEntityToTreeModel(new TreeNodeStructureEntity());
        // then
        Assert.assertNotNull(model);
    }

    @Test
    public void should_ConvertToModel_When_GivenEntityHasData() {
        // given
        UUID uuid = UUID.randomUUID();
        TreeNodeStructureEntity treeNodeStructureEntity = new TreeNodeStructureEntity();
        treeNodeStructureEntity.setId(1L);
        treeNodeStructureEntity.setParent(null);
        treeNodeStructureEntity.setPublicId(uuid.toString());
        treeNodeStructureEntity.setValue(1L);
        treeNodeStructureEntity.setNextLevelNodes(new ArrayList<>());
        // when
        TreeNodeModel model = Converter.convertEntityToNodeModel(treeNodeStructureEntity);
        //then
        assertThat(model.getPublicId(), is(equalTo(uuid.toString())));
        assertThat(model.getParentPublicId(), nullValue());
        assertThat(model.getValue(), is(equalTo(1L)));
        assertThat(model.getSum(), is(equalTo(1L)));
    }

    @Test
    public void should_ConvertToTree_When_GivenEntityHasData() {
        // given
        UUID uuid = UUID.randomUUID();
        TreeNodeStructureEntity treeNodeStructureEntity = new TreeNodeStructureEntity();
        treeNodeStructureEntity.setId(1L);
        treeNodeStructureEntity.setParent(null);
        treeNodeStructureEntity.setPublicId(uuid.toString());
        treeNodeStructureEntity.setValue(1L);
        treeNodeStructureEntity.setNextLevelNodes(new ArrayList<>());
        // when
        TreeStructureModel model = Converter.convertEntityToTreeModel(treeNodeStructureEntity);
        //then
        assertThat(model.getNode().getPublicId(), is(equalTo(uuid.toString())));
        assertThat(model.getNode().getParentPublicId(), nullValue());
        assertThat(model.getNode().getValue(), is(equalTo(1L)));
        assertThat(model.getNode().getSum(), is(equalTo(1L)));
        assertThat(model.getChildren(), emptyCollectionOf(TreeStructureModel.class));
    }

    @Test
    public void should_ConvertToEntity_When_ModelEntityHasData() {
        // given
        UUID uuid = UUID.randomUUID();
        TreeNodeModel model = new TreeNodeModel(null, uuid.toString(), 1L, 1L);
        // when
        TreeNodeStructureEntity entity = Converter.convertNodeModelToEntity(model, repository);
        //then
        assertThat(entity.getPublicId(), is(equalTo(uuid.toString())));
        assertThat(entity.getId(), nullValue());
        assertThat(entity.getValue(), is(equalTo(1L)));
        assertThat(entity.getNextLevelNodes(), emptyCollectionOf(TreeNodeStructureEntity.class));
    }


}