package com.connundrum.treestructure.service.impl;

import com.connundrum.treestructure.model.entity.TreeNodeStructureEntity;
import com.connundrum.treestructure.model.gui.TreeNodeModel;
import com.connundrum.treestructure.model.gui.TreeStructureModel;
import com.connundrum.treestructure.repository.TreeNodeStructureRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.emptyCollectionOf;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TreeStructureServiceImplTest {

    @InjectMocks
    private TreeStructureServiceImpl testedService;

    @Mock
    private TreeNodeStructureRepository repository;

    private final String PUBLIC_ID = UUID.fromString("2f3bc55c-3582-4d47-846a-fa3a5eccc8fd").toString();
    private final TreeNodeStructureEntity defaultEntity = new TreeNodeStructureEntity(1L, PUBLIC_ID, 1L, null, new ArrayList<>());
    private final TreeNodeModel defaultModel = new TreeNodeModel(null, PUBLIC_ID, 1L, 1L);

    @Before
    public void setUp(){
        when(repository.findRoots()).thenReturn(Arrays.asList(defaultEntity));
        when(repository.findByPublicId(PUBLIC_ID)).thenReturn(defaultEntity);
        when(repository.save(Matchers.any(TreeNodeStructureEntity.class))).thenAnswer(new Answer<TreeNodeStructureEntity>() {
            @Override
            public TreeNodeStructureEntity answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                return (TreeNodeStructureEntity) args[0];
            }
        });
    }

    @Test
    public void shouldGetTree_When_WeAskAboutProperPublicId(){
        //given
        //when
        List<TreeStructureModel> model = testedService.get();

        //then
        assertThat(model.size(), is(equalTo(1)));
        assertThat(model.get(0).getNode().getPublicId(), is(equalTo(PUBLIC_ID)));
        assertThat(model.get(0).getNode().getParentPublicId(), nullValue());
        assertThat(model.get(0).getNode().getValue(), is(equalTo(1L)));
        assertThat(model.get(0).getNode().getSum(), is(equalTo(1L)));
        assertThat(model.get(0).getChildren(), emptyCollectionOf(TreeStructureModel.class));
    }

    @Test
    public void shouldGetTreeNode_When_WeAskAboutProperPublicId(){
        //given
        //when
        TreeStructureModel model = testedService.getByPublicId(PUBLIC_ID);

        //then
        assertThat(model.getNode().getPublicId(), is(equalTo(PUBLIC_ID)));
        assertThat(model.getNode().getParentPublicId(), nullValue());
        assertThat(model.getNode().getValue(), is(equalTo(1L)));
        assertThat(model.getNode().getSum(), is(equalTo(1L)));
        assertThat(model.getChildren(), emptyCollectionOf(TreeStructureModel.class));
    }


    @Test
    public void shouldCreateTreeNode_WhenModelIsNotEmpty(){

        //given

        //when
        String publicId  = testedService.createNode(defaultModel);

        //then
        assertThat(publicId, is(equalTo(PUBLIC_ID)));
    }

    @Test
    public void shouldCreateTreeNode_WhenModelIsNotEmptyAndPublicIdIsEmpty(){

        //given
        defaultModel.setPublicId(null);

        //when
        String publicId  = testedService.createNode(defaultModel);

        //then
        assertThat(publicId, notNullValue());

        //clean up
        defaultModel.setPublicId(PUBLIC_ID);
    }


    @Test
    public void shouldUpdateReturnNoErrorMachineParkStructure_When_ItIsNotEmpty(){

        //given
        defaultModel.setValue(2L);

        //when
        testedService.updateNode(defaultModel);

        //then
        defaultModel.setValue(1L);
    }

}