package com.connundrum.treestructure.standalone.cucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources", plugin = "json:target/cucumber/cucumber-report.json")
public class CucumberTest {


}
