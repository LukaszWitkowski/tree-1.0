package com.connundrum.treestructure.standalone.cucumber;

import com.connundrum.treestructure.model.gui.TreeNodeModel;
import com.connundrum.treestructure.model.gui.TreeStructureModel;
import com.connundrum.treestructure.standalone.TreeStructureServiceApplication;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.ObjectMapperConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.parsing.Parser;
import com.jayway.restassured.response.Response;
import cucumber.api.java8.En;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import static com.jayway.restassured.RestAssured.expect;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.Is.is;


@ContextConfiguration(classes = TreeStructureServiceApplication.class, loader = SpringApplicationContextLoader.class)
@WebIntegrationTest({ "server.port=0" })
@ActiveProfiles("test")
public class StepsDefsBasicCRUDTreeStructure implements En {

    @Value("${local.server.port}")
    protected int serverPort;

    @Autowired
    private ObjectMapper objectMapper;

    static Response response;
    static TreeNodeModel lastSendPayload;

    private final String endpointPath = "/treeStructureService/treeStructure";

    public StepsDefsBasicCRUDTreeStructure() {

        RestAssured.defaultParser = Parser.JSON;
        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
                (Class aClass, String s) -> objectMapper
        ));


        When("Create new node with empty parent",
                () -> {
                    lastSendPayload = new TreeNodeModel(null, null, 1L, null);
                    response = expect()
                            .given()
                            .port(serverPort)
                            .contentType(ContentType.JSON)
                            .content(lastSendPayload)
                            .when()
                            .post(endpointPath+"/node");

                    lastSendPayload.setPublicId(response.getHeader("Location"));
                });

        Then("Response returns publicId of this created node",
                () -> {
                    assertThat(response.getStatusCode(), equalTo(HttpStatus.CREATED.value()));
                    assertThat(response.getHeader("Location"), is(notNullValue()));
                });


        When("Get tree structure for created public id",
                () -> {
                    response = expect()
                            .given()
                            .port(serverPort)
                            .when()
                            .get(endpointPath + "/" + lastSendPayload.getPublicId());
                });

        When("Get tree structure for update public id",
                () -> {
                    response = expect()
                            .given()
                            .port(serverPort)
                            .when()
                            .get(endpointPath + "/" + lastSendPayload.getPublicId());
                });



        Then("Response has information about tree structure",
                () -> {
                    TreeStructureModel responseObject = response.getBody().as(TreeStructureModel.class);
                    assertThat(responseObject, is(notNullValue()));
                    assertThat(responseObject.getNode().getPublicId(), is(notNullValue()));
                    assertThat(responseObject.getNode().getValue(), is(equalTo(1L)));
                });

        Then("Response returns tree structure with updated details",
                () -> {
                    TreeStructureModel responseObject = response.getBody().as(TreeStructureModel.class);
                    assertThat(responseObject, is(notNullValue()));
                    assertThat(responseObject.getNode().getPublicId(), is(notNullValue()));
                    assertThat(responseObject.getNode().getValue(), is(equalTo(2L)));
                });


        When("Update tree node",
                () -> {
                    lastSendPayload.setValue(2L);

                    response = expect()
                            .given()
                            .port(serverPort)
                            .contentType(ContentType.JSON)
                            .content(lastSendPayload)
                            .when()
                            .put(endpointPath + "/node/" + lastSendPayload.getPublicId());
                });

        Then("Resposne returns NO_CONTENT",
                () -> {
                    assertThat(response.getStatusCode(), equalTo(HttpStatus.NO_CONTENT.value()));
                });



        Then("NotFound Exception was returned",
                () -> {
                    assertThat(response.getStatusCode(), equalTo(HttpStatus.INTERNAL_SERVER_ERROR.value()));
                });

        When("Delete node created in first scenario",
                () -> {
                    response = expect()
                            .given()
                            .port(serverPort)
                            .when()
                            .delete(endpointPath + "/node/" + lastSendPayload.getPublicId());
                });
    }

}
