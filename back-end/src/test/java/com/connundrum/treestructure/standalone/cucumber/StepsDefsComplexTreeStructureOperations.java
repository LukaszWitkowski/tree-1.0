package com.connundrum.treestructure.standalone.cucumber;


import com.connundrum.treestructure.model.gui.TreeNodeModel;
import com.connundrum.treestructure.model.gui.TreeStructureModel;
import com.connundrum.treestructure.standalone.TreeStructureServiceApplication;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.ObjectMapperConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.parsing.Parser;
import com.jayway.restassured.response.Response;
import cucumber.api.PendingException;
import cucumber.api.java8.En;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import static com.jayway.restassured.RestAssured.expect;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isIn;
import static org.hamcrest.core.Is.is;

@ContextConfiguration(classes = TreeStructureServiceApplication.class, loader = SpringApplicationContextLoader.class)
@WebIntegrationTest({ "server.port=0" })
@ActiveProfiles("test")
public class StepsDefsComplexTreeStructureOperations implements En {

    @Value("${local.server.port}")
    protected int serverPort;

    @Autowired
    private ObjectMapper objectMapper;

    static Response response;
    static TreeNodeModel root, leftChild, rightChild, leafOne, leafTwo, leafThree, leafFour;

    private final String endpointPath = "/treeStructureService/treeStructure";

    public StepsDefsComplexTreeStructureOperations() {

        RestAssured.defaultParser = Parser.JSON;
        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
                (Class aClass, String s) -> objectMapper
        ));


        When("Create full binary tree with values 1 in each node",
                () -> {
                    root = new TreeNodeModel(null, null, 1L, null);
                    response = expect()
                            .given()
                            .port(serverPort)
                            .contentType(ContentType.JSON)
                            .content(root)
                            .when()
                            .post(endpointPath+"/node");

                    root.setPublicId(response.getHeader("Location"));

                    leftChild = new TreeNodeModel(root.getPublicId(), null, 1L, null);
                    response = expect()
                            .given()
                            .port(serverPort)
                            .contentType(ContentType.JSON)
                            .content(leftChild)
                            .when()
                            .post(endpointPath+"/node");

                    leftChild.setPublicId(response.getHeader("Location"));

                    rightChild = new TreeNodeModel(root.getPublicId(), null, 1L, null);
                    response = expect()
                            .given()
                            .port(serverPort)
                            .contentType(ContentType.JSON)
                            .content(rightChild)
                            .when()
                            .post(endpointPath+"/node");

                    rightChild.setPublicId(response.getHeader("Location"));

                    leafOne = new TreeNodeModel(leftChild.getPublicId(), null, 1L, null);
                    response = expect()
                            .given()
                            .port(serverPort)
                            .contentType(ContentType.JSON)
                            .content(leafOne)
                            .when()
                            .post(endpointPath+"/node");

                    leafOne.setPublicId(response.getHeader("Location"));

                    leafTwo = new TreeNodeModel(leftChild.getPublicId(), null, 1L, null);
                    response = expect()
                            .given()
                            .port(serverPort)
                            .contentType(ContentType.JSON)
                            .content(leafTwo)
                            .when()
                            .post(endpointPath+"/node");

                    leafTwo.setPublicId(response.getHeader("Location"));

                    leafThree = new TreeNodeModel(rightChild.getPublicId(), null, 1L, null);
                    response = expect()
                            .given()
                            .port(serverPort)
                            .contentType(ContentType.JSON)
                            .content(leafThree)
                            .when()
                            .post(endpointPath+"/node");

                    leafThree.setPublicId(response.getHeader("Location"));

                    leafFour = new TreeNodeModel(rightChild.getPublicId(), null, 1L, null);
                    response = expect()
                            .given()
                            .port(serverPort)
                            .contentType(ContentType.JSON)
                            .content(leafFour)
                            .when()
                            .post(endpointPath+"/node");

                    leafFour.setPublicId(response.getHeader("Location"));
                });

        Then("All leaves have got sum equal to 3",
                () -> {
                    response = expect()
                            .given()
                            .port(serverPort)
                            .when()
                            .get(endpointPath + "/");

                    TreeStructureModel[] trees = response.getBody().as(TreeStructureModel[].class);
                    assertThat(trees.length, is(equalTo(1)));
                    assertThat(trees[0].getNode().getPublicId(), is(equalTo(root.getPublicId())));
                    trees[0].getChildren().forEach(child -> child.getChildren().forEach(leaf -> {
                        assertThat(leaf.getNode().getSum(), is(equalTo(3L)));
                        assertThat(leaf.getNode().getPublicId(), isIn(Lists.newArrayList
                                (leafOne.getPublicId(),leafTwo.getPublicId(),leafThree.getPublicId(),leafFour.getPublicId())));
                    }));
                });


        When("Change values of nodes from 1 to 7 from top to bottom",
                () -> {
                    leftChild.setValue(2L);
                    response = expect()
                            .given()
                            .port(serverPort)
                            .contentType(ContentType.JSON)
                            .content(leftChild)
                            .when()
                            .put(endpointPath+"/node/"+leftChild.getPublicId());

                    rightChild.setValue(3L);
                    response = expect()
                            .given()
                            .port(serverPort)
                            .contentType(ContentType.JSON)
                            .content(rightChild)
                            .when()
                            .put(endpointPath+"/node/"+rightChild.getPublicId());


                    leafOne.setValue(4L);
                    response = expect()
                            .given()
                            .port(serverPort)
                            .contentType(ContentType.JSON)
                            .content(leafOne)
                            .when()
                            .put(endpointPath+"/node/"+leafOne.getPublicId());

                    leafTwo.setValue(5L);
                    response = expect()
                            .given()
                            .port(serverPort)
                            .contentType(ContentType.JSON)
                            .content(leafTwo)
                            .when()
                            .put(endpointPath+"/node/"+leafTwo.getPublicId());

                    leafThree.setValue(6L);
                    response = expect()
                            .given()
                            .port(serverPort)
                            .contentType(ContentType.JSON)
                            .content(leafThree)
                            .when()
                            .put(endpointPath+"/node/"+leafThree.getPublicId());

                    leafFour.setValue(7L);
                    response = expect()
                            .given()
                            .port(serverPort)
                            .contentType(ContentType.JSON)
                            .content(leafFour)
                            .when()
                            .put(endpointPath+"/node/"+leafFour.getPublicId());

                });


        Then("Leaves values have appropriate sums",
                () -> {
                    response = expect()
                            .given()
                            .port(serverPort)
                            .when()
                            .get(endpointPath + "/");

                    TreeStructureModel[] trees = response.getBody().as(TreeStructureModel[].class);
                    long[] counter = new long[1];
                    counter[0]=0L;
                    assertThat(trees.length, is(equalTo(1)));
                    assertThat(trees[0].getNode().getPublicId(), is(equalTo(root.getPublicId())));
                    trees[0].getChildren().forEach(child -> {
                        child.getChildren().forEach(leaf -> {
                                assertThat(leaf.getNode().getSum(), is(equalTo(7L + counter[0])));
                                counter[0]++;
                                assertThat(leaf.getNode().getPublicId(), isIn(Lists.newArrayList
                                        (leafOne.getPublicId(), leafTwo.getPublicId(), leafThree.getPublicId(), leafFour.getPublicId())));
                        });
                        counter[0]++;
                    });
                });

        When("^Try to transplant node to its subtree$",
                () -> {
                   rightChild.setParentPublicId(leafFour.getPublicId());
                   response = expect()
                            .given()
                            .port(serverPort)
                            .contentType(ContentType.JSON)
                            .content(rightChild)
                            .when()
                            .put(endpointPath+"/node/"+rightChild.getPublicId());
                    //clean up
                    rightChild.setParentPublicId(root.getPublicId());
                });

        Then("We get INTERNAL_ERROR",
                () -> {
                    assertThat(response.getStatusCode(), equalTo(HttpStatus.INTERNAL_SERVER_ERROR.value()));
                });

        When("^Transplant right root subtree to left root subtree leaf$", () -> {
            rightChild.setParentPublicId(leafOne.getPublicId());
            response = expect()
                    .given()
                    .port(serverPort)
                    .contentType(ContentType.JSON)

                    .content(rightChild)
                    .when()
                    .put(endpointPath+"/node/"+rightChild.getPublicId());
        });

        Then("^Tree has high (\\d+) and (\\d+) nodes in total$", (Long high, Long nodesNumber) -> {
            response = expect()
                    .given()
                    .port(serverPort)
                    .when()
                    .get(endpointPath + "/");

            TreeStructureModel[] trees = response.getBody().as(TreeStructureModel[].class);
            long[] counter = new long[1];
            counter[0]=1L;
            assertThat(trees.length, is(equalTo(1)));
            assertThat(trees[0].getNode().getPublicId(), is(equalTo(root.getPublicId())));
            countNodes(trees[0], counter);
            assertThat(counter[0], is(equalTo(nodesNumber)));
            });



        When("^Delete node on (\\d+) level$", (Integer level) -> {
            response = expect()
                    .given()
                    .port(serverPort)
                    .when()
                    .delete(endpointPath + "/node/" + rightChild.getPublicId());

            assertThat(response.getStatusCode(), equalTo(HttpStatus.NO_CONTENT.value()));
        });

        Then("^Root has only one child and there are (\\d+) nodes in total$", (Long nodesNumber) -> {
            response = expect()
                    .given()
                    .port(serverPort)
                    .when()
                    .get(endpointPath + "/");

            TreeStructureModel[] trees = response.getBody().as(TreeStructureModel[].class);
            long[] counter = new long[1];
            counter[0]=1L;
            assertThat(trees.length, is(equalTo(1)));
            assertThat(trees[0].getNode().getPublicId(), is(equalTo(root.getPublicId())));
            countNodes(trees[0], counter);
            assertThat(counter[0], is(equalTo(nodesNumber)));
        });

        When("^Change parent for leaves nodes$", () -> {
            leafOne.setParentPublicId(root.getPublicId());
            response = expect()
                    .given()
                    .port(serverPort)
                    .contentType(ContentType.JSON)
                    .content(leafOne)
                    .when()
                    .put(endpointPath+"/node/"+leafOne.getPublicId());
            leafTwo.setParentPublicId(root.getPublicId());
            response = expect()
                    .given()
                    .port(serverPort)
                    .contentType(ContentType.JSON)
                    .content(leafTwo)
                    .when()
                    .put(endpointPath+"/node/"+leafTwo.getPublicId());
        });

        Then("^All nodes are root childs$", () -> {
            response = expect()
                    .given()
                    .port(serverPort)
                    .when()
                    .get(endpointPath + "/");

            TreeStructureModel[] trees = response.getBody().as(TreeStructureModel[].class);
            long[] counter = new long[1];
            counter[0]=1L;
            assertThat(trees.length, is(equalTo(1)));
            assertThat(trees[0].getNode().getPublicId(), is(equalTo(root.getPublicId())));
            trees[0].getChildren().forEach(child -> {
                assertThat(child.getChildren().size(), is(equalTo(0)));
            });
        });

    }

    private void countNodes(TreeStructureModel trees, long[] counter) {
         trees.getChildren().forEach(child -> {
                counter[0]++;
                countNodes(child, counter);
            });
    }

}

