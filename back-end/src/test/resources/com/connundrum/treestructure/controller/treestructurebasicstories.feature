Feature: Basic CRUD operations

  Scenario: Create a new node
    When Create new node with empty parent
    Then Response returns publicId of this created node

  Scenario: Get tree structure by public id
    When Get tree structure for created public id
    Then Response has information about tree structure

  Scenario: Update node
    When Update tree node
    Then Resposne returns NO_CONTENT
    When Get tree structure for update public id
    Then Response returns tree structure with updated details

  Scenario: Delete tree node
    When Delete node created in first scenario
    Then Resposne returns NO_CONTENT message

  Scenario: Get not existing tree structure by public id
    When Get tree structure for created public id
    Then NotFound Exception was returned

  Scenario: Delete non existing tree node
    When Delete node created in first scenario
    Then NotFound Exception was returned





