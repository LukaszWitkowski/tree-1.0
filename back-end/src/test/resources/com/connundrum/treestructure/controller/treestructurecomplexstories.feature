Feature: Complex tree structure operations

  Scenario: Create a full binary tree of high 2 and manipulate nodes values
    Given Create full binary tree with values 1 in each node
    Then All leaves have got sum equal to 3
    When Change values of nodes from 1 to 7 from top to bottom
    Then Leaves values have appropriate sums

  Scenario: Transplant right root child to one of his own childs
    When Try to transplant node to its subtree
    Then We get INTERNAL_ERROR

  Scenario: Transplant right root child to one of left root child children
    When Transplant right root subtree to left root subtree leaf
    Then Tree has high 4 and 7 nodes in total

  Scenario: Delete subtree on level 3 and below
    When Delete node on 3 level
    Then Root has only one child and there are 4 nodes in total

  Scenario: Move nodes from leaves to root
    When Change parent for leaves nodes
    Then All nodes are root childs
